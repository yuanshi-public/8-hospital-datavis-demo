/**
 * 设备分布
 */
const carParkingStat = {
  carEntryQnty: 50,
  carExitQnty: 20,
  freeParkingSpots: 5,
};

/**
 * 巡更
 */
const watchData = [
  {
    id: '123',
    patrolEquip: '巡更器AG1',
    personnel: '张安',
    location: '新发热门诊1楼',
    time: '2022-03-09 11:03:10',
    events: '',
  },
  {
    id: '124',
    patrolEquip: '巡更器BGH',
    personnel: '李恩',
    location: '住院楼3楼',
    time: '2022-03-09 11:03:10',
    events: '',
  },
  {
    id: '125',
    patrolEquip: '巡更器B1R',
    personnel: '李耳',
    location: '门诊楼5楼',
    time: '2022-03-09 11:03:10',
    events: '',
  },
  {
    id: '126',
    patrolEquip: '巡更器L1',
    personnel: '唐果',
    location: '行政楼1楼',
    time: '2022-03-09 11:03:10',
    events: '',
  },
  {
    id: '127',
    patrolEquip: '巡更器M2',
    personnel: '郑钧',
    location: '肝病楼2楼',
    time: '2022-03-09 11:03:10',
    events: '',
  },
];
