/**
 * 抬头
 */
const title = '西安市第八医院';

/**
 * logo
 */

const logo = 'imgs/logo.png';

/**
 * 地图中点 经度 纬度
 */
const mapCenter = [108.94236, 34.196633];

/**
 * webRTC srs服务器的IP地址
 */
const srsServerIP = '192.168.1.176';
/**
 * webRTC srs服务器的端口
 */
const srsServerPort = '1985';

/**
 * 请求URL
 */
const apiBaseURL = 'http://192.168.1.150:10015';

/**
 * 热力图max人密度
 */
const maxPeopleDensity = 500;

/**
 * 速率单位
 */
const speedUnit = 'kB/s';

/**
 * 主拓扑图id
 */
const mainTopoId = '123';
const mainTopoName = '综合拓扑';
