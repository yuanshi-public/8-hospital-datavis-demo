/**
 * 访客对比 今日
 */
const visitorsDataDay = [
  {name: '参观', value: 60},
  {name: '商务', value: 80},
  {name: '应聘', value: 75},
  {name: '其他', value: 40},
];

const visitorsDataWeek = [
  {name: '参观', value: 140},
  {name: '商务', value: 180},
  {name: '应聘', value: 156},
  {name: '其他', value: 60},
];

const visitorsDataMonth = [
  {name: '参观', value: 300},
  {name: '商务', value: 360},
  {name: '应聘', value: 320},
  {name: '其他', value: 150},
]; 
